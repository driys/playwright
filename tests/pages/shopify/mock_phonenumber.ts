import { Page } from "@playwright/test";
export default class MockPhone{

    constructor(public page: Page){ }

        async enterPhoneNumber(phonenumber: string){
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByPlaceholder('Nomor handphone').click();
            await this.page.waitForTimeout(1000);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByPlaceholder('Nomor handphone').fill(phonenumber);
            await this.page.waitForTimeout(1000);
        }

        async clickFieldEmail(){
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByPlaceholder('Email').click();
        }

        async enterOTP(otp1: string, otp2: string, otp3: string, otp4: string){
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('textbox', { name: 'Please enter verification code. Digit 1' }).fill(otp1);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('textbox', { name: 'Digit 2' }).fill(otp2);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('textbox', { name: 'Digit 3' }).fill(otp3);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('textbox', { name: 'Digit 4' }).fill(otp4);
        }

        async selectCourier(){
            await this.page.waitForTimeout(1000);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').locator('div:nth-child(3) > .w-3\\.5').click();
            await this.page.waitForTimeout(2000);
            if (await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('J&T - Regular').nth(1).isVisible()){
                await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('J&T - Regular').nth(1).click();
            } else if (await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('TIKI - Regular').nth(1).isVisible()) {
                await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('TIKI - Regular').nth(1).click();
            } else if (await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Ninja Xpress - Regular').nth(1).isVisible()){
                await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Ninja Xpress - Regular').nth(1).click();
            } else {
                stop();
            }
            
        }

}