import { Page } from "@playwright/test";
import { test, expect } from '@playwright/test';
export default class SelectItem{

    constructor(public page: Page){ }

        async buyItNow(){
            await this.page.goto('https://payable-merch-staging.myshopify.com/');
            await this.page.getByRole('link', { name: 'Cargo pants' }).click();
            await this.page.waitForTimeout(1000);
            await this.page.getByRole('button', { name: 'Increase quantity for Cargo pants' }).click();
            await this.page.waitForTimeout(1000);
            await this.page.getByRole('button', { name: 'Buy it now' }).click();
        }

        async addToCart(){
            await this.page.goto('https://payable-merch-staging.myshopify.com/');
            await this.page.getByRole('link', { name: 'Payable Bomber' }).click();
            await this.page.getByText('LargeVariant sold out or unavailable').click();
            await this.page.getByText('WhiteVariant sold out or unavailable').click();
            await this.page.waitForTimeout(2000);
            await this.page.getByRole('button', { name: 'Increase quantity for Payable Bomber' }).click();
            await this.page.waitForTimeout(2000);
            await this.page.getByRole('button', { name: 'Add to cart' }).click();
            await this.page.waitForTimeout(2000);
            await this.page.getByRole('button', { name: 'Close' }).click();
            await this.page.waitForTimeout(2000);
            await this.page.getByRole('link', { name: 'Cart' }).click();
            await this.page.waitForTimeout(2000);
            await expect(this.page.getByRole('table', { name: 'Your cart' }).getByRole('link', { name: 'Payable Bomber' })).toHaveCount(2);
            await expect.soft(this.page.getByRole('cell', { name: 'Rp 900.000,00' }).getByText('Rp 900.000,00')).toHaveText(/Rp 900.000,00/);
            await this.page.getByRole('button', { name: 'Check out' }).click();
        }
}
