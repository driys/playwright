import { Page, expect } from "@playwright/test";
export default class CreateLink{

    constructor(public page: Page){ }

        async redirectToLinkPage(){
            await this.page.getByRole('link', { name: 'Links' }).click();
            await expect(this.page.getByRole('heading', { name: 'Links' })).toBeVisible();
            await this.page.getByRole('button', { name: 'Reusable links' }).click();
            await this.page.getByRole('button', { name: 'Single-use links' }).click();
        }

        async enterNamaLink(nama: string){
            await this.page.getByPlaceholder('Nama link').click();
            await this.page.getByPlaceholder('Nama link').fill(nama);
        }

        async enterPaymentTimelimit(hari: string, jam: string, menit: string){
            await this.page.getByLabel('Hari').click();
            await this.page.getByLabel('Hari').fill(hari);
            await this.page.getByLabel('Jam').click();
            await this.page.getByLabel('Jam').fill(jam);
            await this.page.getByLabel('Menit').click();
            await this.page.getByLabel('Menit').fill(menit);
        }

        async addItem(){
            if(await this.page.getByRole('button', { name: 'Tambah item' }).isVisible()){
                await this.page.getByRole('button', { name: 'Tambah item' }).click();
            } else {
                await this.page.getByRole('button', { name: 'Pilih item' }).click();
            }
            await this.page.getByText('Cargo pants').click();
            await this.page.waitForTimeout(2000);
            await this.page.locator('div:nth-child(11) > div:nth-child(2) > .flex').click();
            if(await this.page.getByRole('button', { name: 'Tambahkan' }).isVisible()){
                await this.page.getByRole('button', { name: 'Tambahkan' }).click();
            }
            // await this.page.getByPlaceholder('Cari produk').click();
            // await this.page.getByPlaceholder('Cari produk').fill('cargo pants');
            // // await this.page.getByRole('button', { name: 'Pilih' }).click();
            // await this.page.locator('//*[@id="headlessui-dialog-panel-:rr:"]/div[4]/div/div[2]/button').click();
        }

        async chooseDate(){
            await this.page.getByRole('button', { name: 'Choose date' }).click();
            await this.page.getByRole('button', { name: 'calendar view is open, switch to year view' }).click();
            await this.page.getByRole('button', { name: '2024' }).click();
            await this.page.getByRole('gridcell', { name: '20' }).click();
        }

        async checkCreatedSingleLink(){
            await this.page.getByRole('button', { name: 'Buat' }).click();
            const toast = this.page.getByText('Single use link successfully created');
            await expect(toast).toBeVisible();
            await this.page.getByRole('button', { name: 'Dismiss' }).click();
        }

        async checkCreatedReusableLink(){
            await this.page.getByRole('button', { name: 'Buat' }).click();
            const toast = this.page.getByText('Reusable link successfully created');
            await expect(toast).toBeVisible();
            await this.page.getByRole('button', { name: 'Dismiss' }).click();
        }
}
