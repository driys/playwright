import { test, expect } from '@playwright/test';
import LoginPage from '../../../pages/dashboard/login';
import CreateLink from '../../../pages/dashboard/create_link';

test.describe.parallel('Create link', () => {
   
    test('Single use link with delivery', async ({ page }) => {
        const Login = new LoginPage(page);
        // Login
        
        await Login.login('gerywahyunugraha@gmail.com', 'ger1wahyunugraha');
        await page.pause();

        // Goto Link Page
        const Link = new CreateLink(page);
        await Link.redirectToLinkPage();

        // Create new link
        await page.getByRole('button', { name: 'Buat Link Baru' }).click();
        await page.getByRole('button', { name: 'Buat' }).click();
        await page.waitForTimeout(2000);
        await Link.enterNamaLink('Auto Single Use');
        await Link.enterPaymentTimelimit('2','5','30');
        await Link.addItem();
        await Link.checkCreatedSingleLink();
    });

    test('Single use link without delivery', async ({ page }) => {
        const Login = new LoginPage(page);
        // Login
        await Login.login('gerywahyunugraha@gmail.com', 'ger1wahyunugraha');
        await page.pause();

        // Goto Link Page
        const Link = new CreateLink(page);
        await Link.redirectToLinkPage();

        // Create new link
        await page.getByRole('button', { name: 'Buat Link Baru' }).click();
        await page.getByRole('radio', { name: 'Tanpa pengiriman' }).click();
        await page.getByRole('button', { name: 'Buat' }).click();
        await Link.enterNamaLink('Auto Single Use');
        await Link.enterPaymentTimelimit('1','1','20');
        await Link.addItem();
        await Link.checkCreatedSingleLink();
    });

    test('Reusable link with delivery', async ({ page }) => {
        const Login = new LoginPage(page);
        // Login
        await Login.login('gerywahyunugraha@gmail.com', 'ger1wahyunugraha');
        await page.pause();

        // Goto Link Page
        const Link = new CreateLink(page);
        await Link.redirectToLinkPage();

        // Create new link
        await page.getByRole('button', { name: 'Buat Link Baru' }).click();
        await page.getByRole('radio', { name: 'Reusable' }).click();
        await page.getByRole('button', { name: 'Buat' }).click();
        await Link.enterNamaLink('Reusable Link');
        // Link active immediately
        await page.locator('[id="headlessui-switch-:rl:"]').click();

        await Link.enterPaymentTimelimit('1','1','20');
        await Link.addItem();
        await Link.checkCreatedReusableLink();
    });

    test('Reusable link without delivery', async ({ page }) => {
        const Login = new LoginPage(page);
        // Login
        await Login.login('gerywahyunugraha@gmail.com', 'ger1wahyunugraha');
        await page.pause();

        // Goto Link Page
        const Link = new CreateLink(page);
        await Link.redirectToLinkPage();

        // Create new link
        await page.getByRole('button', { name: 'Buat Link Baru' }).click();
        await page.getByRole('radio', { name: 'Reusable' }).click();
        await page.getByRole('radio', { name: 'Tanpa pengiriman' }).click();
        await page.getByRole('button', { name: 'Buat' }).click();
        await Link.enterNamaLink('Reusable Link');
        // Link active immediately
        await page.locator('[id="headlessui-switch-:rl:"]').click();

        await Link.enterPaymentTimelimit('1','1','20');
        await Link.addItem();
        await Link.checkCreatedReusableLink();
    });
});