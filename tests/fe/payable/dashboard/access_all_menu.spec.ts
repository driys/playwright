import { test, expect } from '@playwright/test';
import LoginPage from '../../../pages/dashboard/login';

test('test', async ({ page }) => {
  
  const Login = new LoginPage(page);
  
  // Login
  await Login.login('gerywahyunugraha@gmail.com', 'ger1wahyunugraha');

  // Check semua menu dapat diakses
  await page.getByRole('button', { name: 'Semua' }).click();
  await page.getByRole('button', { name: 'Menunggu Dibayar' }).click();
  await page.getByRole('button', { name: 'Perlu Konfirmasi' }).click();
  await page.getByRole('button', { name: 'Perlu Dikirim' }).click();
  await page.getByRole('button', { name: 'Sedang Dikirim' }).click();
  await page.getByRole('button', { name: 'Selesai' }).click();
  await page.getByRole('button', { name: 'Dibatalkan' }).click();
  await page.getByRole('link', { name: 'Links' }).click();
  await page.getByRole('button', { name: 'Reusable links' }).click();
  await page.getByRole('link', { name: 'Products' }).click();
  await page.getByRole('button', { name: 'Collection' }).click();
  await page.getByRole('link', { name: 'Customers' }).click();
  await page.getByRole('link', { name: 'Financials' }).click();
  await page.getByRole('link', { name: 'Settings' }).click();
});