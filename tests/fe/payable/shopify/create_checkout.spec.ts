import { test, expect } from '@playwright/test';

test.describe.parallel('Create a checkout in payable-staging Shopify', () => {
    
    
    test('Create a checkout from buy now', async ({ page }) => {
        await page.goto('https://payable-merch-staging.myshopify.com/');
        await page.pause();
        await page.getByRole('link', { name: 'Payable Bomber' }).click();
        await page.getByText('MediumVariant sold out or unavailable').click();
        await page.getByText('WhiteVariant sold out or unavailable').click();
        await page.getByRole('button', { name: 'Increase quantity for Payable Bomber' }).click();
        await page.getByRole('button', { name: 'Increase quantity for Payable Bomber' }).click();
        await page.getByRole('button', { name: 'Buy it now' }).click();
        // Check iframe to Have matched Text
        const locator1 = page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('paragraph').filter({ hasText: 'Rp1.350.000' });
        await expect(locator1).toHaveText('Rp1.350.000');
    });


    test('Create a checkout from cart', async ({ page }) => {
        await page.goto('https://payable-merch-staging.myshopify.com/');
        await page.pause();
        await page.getByRole('link', { name: 'Payable Bomber' }).click();
        await page.getByText('LargeVariant sold out or unavailable').click();
        await page.getByText('WhiteVariant sold out or unavailable').click();
        await page.getByRole('button', { name: 'Increase quantity for Payable Bomber' }).click();
        await page.getByRole('button', { name: 'Add to cart' }).click();
        await page.getByRole('link', { name: 'Cart' }).click();
        await expect(page.getByRole('table', { name: 'Your cart' }).getByRole('link', { name: 'Payable Bomber' })).toHaveCount(2);
        await expect.soft(page.getByRole('cell', { name: 'Rp 900.000,00' }).getByText('Rp 900.000,00')).toHaveText(/Rp 900.000,00/);
    });

});