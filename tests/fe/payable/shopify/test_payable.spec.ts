import { test, expect } from '@playwright/test';

test('has title', async ({ page }) => {
  await page.goto('https://www.payable.id/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Payable/);
});

test('get started link', async ({ page }) => {
  await page.goto('https://www.payable.id/');

  // Click the get started link.
  await page.getByRole('link', { name: 'One Click Checkout' }).click();

  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*#occ/);
});
