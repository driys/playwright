import { PlaywrightTestConfig } from '@playwright/test';

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
// require('dotenv').config();

/**
 * See https://playwright.dev/docs/test-configuration.
 */

const config: PlaywrightTestConfig = {
  timeout: 60000,
  retries: 0,
  testDir: './tests/api',
  use: {
    headless: true,
    viewport: { width:1200, height: 720},
    actionTimeout: 10000,
    ignoreHTTPSErrors: true,
    video: 'off',
    screenshot: 'off',
    extraHTTPHeaders: {
      "Authorization": "Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwaWQiOjI0LCJleHAiOjE2Nzk2MzUwMzgsInN0ciI6MTY3OTYzMTQzOCwiYXBwIjoiZXlKcGRpSTZJa0p6UVRnelJtSndWM2haU1ZjcmVqRk5keXRLWW5jOVBTSXNJblpoYkhWbElqb2lkVUo1Y0RWQ2N6TlRYQzlRTjBOUlJsSmFiM0JNVFd3MGJIZDRNRGxRTjNWMVp5dGFXa2QyUW5oWFp6VlVWVzlXWjFGa1JFMDBNRk5VYUV0RldqRXhabHd2SWl3aWJXRmpJam9pTVRFeFlXUmhaR00yWWpNMk5UVTFNVFk1TlRObU56SmpZalJtTmpZNFkyUmpNR1F5WmpRNU16WmhNV0V5TldZMVpXSXhNVFJrTkRFME16azNOMkV5WmlKOSIsInNjayI6ImV5SnBkaUk2SWsxUVNEWk1NbnByVFZaUk4xRndRalZaY1dwd2EzYzlQU0lzSW5aaGJIVmxJam9pUldob1pqa3lhREZpZEVZeVRVUlZVMDVuV0N0SFVteENPVTh4VkhsRmJVSXpjWGhUVFZWdVJHOHlOazl6YVRWbVJpdDVkV2RFWlVwcGRqRm9USE5WYlNJc0ltMWhZeUk2SWpNME1HVTFNekV4TXpWbVpUbGxaRFJsTkRkaU0yUXlNV1JoTTJReFpERTROV1F6TXpjMllUUXlPV1l6WlRrelpXSXlOR0l4TnpFME1qQmlabVkyT1RBaWZRPT0ifQ.4k5QXLgN_4NeVx6opDPMqPEXWr66IPHrh-pgJddye1o"
    }
  },
  projects: [
      {
        name: 'chromium',
        use: { browserName: 'Chromium' },
      },
  
      {
        name: 'firefox',
        use: { browserName: 'Desktop Firefox' },
      },
  
      {
        name: 'webkit',
        use: { browserName: 'webkit' },
      },
  ],
}

export default config